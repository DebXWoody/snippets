#include <readline/history.h>
#include <readline/readline.h>
#include <stdio.h>
#include <stdlib.h>

static int _inp_rl_down_arrow_handler(int count, int key);

char *x = NULL;

static int _inp_rl_down_arrow_handler(int count, int key) {
  x = malloc(strlen(rl_line_buffer));
  strcpy(x, rl_line_buffer);
  rl_replace_line("", 0);
  rl_redisplay();
  rl_done = 1;

  return 0;
}

int main() {
  rl_bind_keyseq("\\e[1;5B", _inp_rl_down_arrow_handler); // ctrl+arrow down
  char *s;
  while ((s = readline(": ")) != NULL) {
    if (strlen(s) > 1) {
      add_history(s);
    }
    if (x != NULL) {
      add_history(x);
      x = NULL;
    }
  }
  return 0;
}
