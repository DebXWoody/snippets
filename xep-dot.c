// gcc `pkg-config --libs --cflags libxml-2.0` -o xep-dot xep-dot.c
// ./xep-dot > xep-dot.dot
// dot -Tpng xep-dot.dot > xep-dot.png

#define _GNU_SOURCE

#include <assert.h>
#include <dirent.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

void parsexml(char *filename) {
  xmlTextReaderPtr reader;
  xmlDocPtr doc;
  xmlNode *root_element = NULL;
  int ret;
  xmlSubstituteEntitiesDefault(1);
  reader = xmlReaderForFile(filename, NULL, 0);
  if (reader == NULL) {
    printf("reader is nul\n");
    return;
  }
  ret = xmlTextReaderRead(reader);
  doc = xmlTextReaderCurrentDoc(reader);
  xmlFreeTextReader(reader);

  if (doc == NULL) {
    printf("doc is null\n");
    return;
  }
  xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
  assert(xpathCtx);

  char *title = NULL;
  char *number = NULL;
  char *status = NULL;

  xmlXPathObjectPtr xpathObj =
      xmlXPathEvalExpression("//xep//header//title", xpathCtx);
  assert(xpathObj);
  if (xpathObj->nodesetval->nodeNr == 0) {
    return;
  }
  if (xpathObj != NULL && xpathObj->nodesetval != NULL &&
      xpathObj->nodesetval->nodeTab[0] != NULL) {
    if (xpathObj->nodesetval->nodeTab[0]->children != NULL &&
        xpathObj->nodesetval->nodeTab[0]->children->content != NULL) {
      asprintf(&title, "%s",
               xpathObj->nodesetval->nodeTab[0]->children->content);
    }
  }
  xpathObj = xmlXPathEvalExpression("//xep//header//number", xpathCtx);
  if (xpathObj->nodesetval->nodeNr == 0) {
    return;
  }
  if (xpathObj != NULL && xpathObj->nodesetval != NULL &&
      xpathObj->nodesetval->nodeTab[0] != NULL) {
    if (xpathObj->nodesetval->nodeTab[0]->children != NULL &&
        xpathObj->nodesetval->nodeTab[0]->children->content != NULL) {
      asprintf(&number, " %s",
               xpathObj->nodesetval->nodeTab[0]->children->content);
    }
  }
  xpathObj = xmlXPathEvalExpression("//xep//header//status", xpathCtx);
  if (xpathObj->nodesetval->nodeNr == 0) {
    return;
  }
  if (xpathObj != NULL && xpathObj->nodesetval != NULL &&
      xpathObj->nodesetval->nodeTab[0] != NULL) {
    if (xpathObj->nodesetval->nodeTab[0]->children != NULL &&
        xpathObj->nodesetval->nodeTab[0]->children->content != NULL) {
      asprintf(&status, "%s",
               xpathObj->nodesetval->nodeTab[0]->children->content);
    }
  }

  if (status != NULL) {

    if (strcmp(status, "Deferred") == 0) {
      printf("%s [shape=box,style=filled,color=\"blue\"];\n", number, title);
    } else if (strcmp(status, "Draft") == 0) {
      printf("%s [shape=box,style=filled,color=\".7 .3 1.0\"];\n", number,
             title);
    } else if (strcmp(status, "Proposed") == 0) {
      printf("%s [shape=box,style=filled,color=\"cyan\"];\n", number, title);
    } else if (strcmp(status, "Retracted") == 0) {
      printf("%s [shape=box,style=filled,color=\".3 .4 1.0\"];\n", number,
             title);
    } else if (strcmp(status, "Obsolete") == 0) {
      printf("%s [shape=box,style=filled,color=\"red\"];\n", number, title);
    } else if (strcmp(status, "Rejected") == 0) {
      printf("%s [shape=box,style=filled,color=\"magenta\"];\n", number, title);
    } else if (strcmp(status, "Experimental") == 0) {
      printf("%s [shape=box,style=filled,color=\".7 .3 4.0\"];\n", number,
             title);
    } else if (strcmp(status, "Active") == 0) {
      printf("%s [shape=box,style=filled,color=\".7 .3 3.0\"];\n", number,
             title);
    }
  }
  if (xpathCtx) {
    xmlXPathFreeContext(xpathCtx);
  }
  if (doc) {
    xmlFreeDoc(doc);
  }
}

int main(int argc, char *argv[]) {
  printf("digraph G {\n");

  DIR *dir = opendir(".");
  for (struct dirent *d = readdir(dir); d != NULL; d = readdir(dir)) {
    regex_t x;
    char *regex = "xep-[[:digit:]][[:digit:]][[:digit:]][[:digit:]]\\.xml";
    regcomp(&x, regex, 0);
    if (!regexec(&x, d->d_name, 0, NULL, 0)) {
      parsexml(d->d_name);
    }
  }
  printf("}\n");
  return EXIT_SUCCESS;
}
