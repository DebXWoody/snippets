/*
 * @readspell.c
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * gcc -g -lreadline -laspell -o readspell readspell.c
 *
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <readline/readline.h>
#include <aspell.h>
#include <string.h>

int h (int count, int key) {
  if (key == ' ') {
    AspellConfig * spell_config = new_aspell_config();
    aspell_config_replace(spell_config, "lang", "en_US");
    AspellCanHaveError * possible_err = new_aspell_speller(spell_config);
    AspellSpeller * spell_checker = 0;
    if (aspell_error_number(possible_err) != 0)
      puts(aspell_error_message(possible_err));
    else
      spell_checker = to_aspell_speller(possible_err);

    char* line = strdupa(rl_line_buffer);
    char* word;
    char* last;
    char *saveptr;
    for (;;) {
      word = strtok_r(line, " ", &saveptr);
      if(word == NULL) {
        break;
      }
      last = word;
      line = NULL;
    }

    int correct = aspell_speller_check(spell_checker, last, strlen(last));
    if(correct == 0 ) {
//      rl_delete_text(strlen(rl_line_buffer) - strlen(last), strlen(rl_line_buffer));
//      rl_insert_text("\x1b[31m");
      rl_insert_text(last);
//      rl_insert_text("\x1b[m");
    }
    rl_insert_text(" ");
    delete_aspell_speller(spell_checker);
  }

}

int main (int argc, char* argv[] ) {


  rl_bind_key (' ',h);

	char * line;
	while ( 1 ) {
		line = readline("\x1b[34m" "prompt>" "\x1b[m ");
	}

	return EXIT_SUCCESS;
}
