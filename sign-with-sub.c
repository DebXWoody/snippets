#include <stdlib.h>
#include <stdio.h>
#include <gpgme.h>
#include <string.h>
#include <locale.h>

/* gpg --quick-gen-key  mail@domain.tld future-default 
 gpg --quick-add-key 1AB89C0E88B55E1F2D89BD923694E9D69B45CFB4 future-default sign

 gpg --fingerprint --fingerprint 1AB89C0E88B55E1F2D89BD923694E9D69B45CFB4
 pub   ed25519 2021-06-21 [SC] [expires: 2023-06-21]
      1AB8 9C0E 88B5 5E1F 2D89  BD92 3694 E9D6 9B45 CFB4
uid           [ultimate] mail@domain.tld
sub   cv25519 2021-06-21 [E]
      CB51 EF52 8BB1 F122 717D  7B20 B8D3 6D58 E3A3 A5A1
sub   ed25519 2021-06-21 [S]
      4875 4269 866F 6980 A3CB  B89C CF11 E065 888C 8A9B
*/

//  gcc sign-with-sub.c `gpgme-config --libs --cflags` -o sign-with-sub

#define MY_KEY  "1AB89C0E88B55E1F2D89BD923694E9D69B45CFB4"
//#define MY_KEY  "48754269866F6980A3CBB89CCF11E065888C8A9B"
#define MESSAGE "Neomutt and XMPP profanity is the best!"

int main(int argc, char* argv[]) {

	const char libversion = gpgme_check_version(NULL);
    printf("GPG: Found gpgme version: %s", libversion);
    gpgme_set_locale(NULL, LC_CTYPE, setlocale(LC_CTYPE, NULL));
 
    gpgme_ctx_t ctx;
    gpgme_error_t error = gpgme_new(&ctx);
    if (GPG_ERR_NO_ERROR != error) {
        printf("gpgme_new: %d\n", error);
        return -1;
    }

    error = gpgme_set_protocol(ctx, GPGME_PROTOCOL_OPENPGP);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
    }

    gpgme_set_armor(ctx, 1);
    gpgme_set_textmode(ctx, 0);
    gpgme_set_offline(ctx, 1);
    gpgme_set_keylist_mode(ctx, GPGME_KEYLIST_MODE_LOCAL);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
    }

    gpgme_key_t sign[3];
    sign[0] = NULL,
    sign[1] = NULL;

    gpgme_signers_clear(ctx);

    error = gpgme_op_keylist_start(ctx, MY_KEY, 1);
    if (error == GPG_ERR_NO_ERROR) {
        error = gpgme_op_keylist_next(ctx, &sign[0]);
        if (error != GPG_ERR_EOF && error != GPG_ERR_NO_ERROR) {
            printf("gpgme_op_keylist_next %s %s", gpgme_strsource(error), gpgme_strerror(error));
            return -1;
        }
    }

	printf("Sign with %s\n", sign[0]->uids->email);
    if (error != 0) {
        printf("Key not found for %s. GpgME Error: %s", sign[0]->uids->email, gpgme_strerror(error));
        return -1;
    }

    error = gpgme_signers_add(ctx, sign[0]);
    if (error != 0) {
        printf("gpgme_signers_add %s. GpgME Error: %s", sign[0]->uids->email, gpgme_strerror(error));
        return -1;
    }

    sign[1] = NULL;

    gpgme_encrypt_flags_t flags = 0;

    gpgme_data_t plain;
    gpgme_data_t signedm;

    error = gpgme_data_new(&plain);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
        return -1;
    }

    error = gpgme_data_new_from_mem(&plain, MESSAGE, strlen(MESSAGE), 0);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
        return -1;
    }
    error = gpgme_data_new(&signedm);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
        return -1;
    }

	error = gpgme_op_sign (ctx, plain, signedm, GPGME_SIG_MODE_CLEAR);
    if (error != 0) {
        printf("GpgME Error: %s", gpgme_strerror(error));
        return -1;
    }

    size_t len;
    char* sign_str = gpgme_data_release_and_get_mem(signedm, &len);
	printf("%s\n", sign_str);

    gpgme_key_release(sign[0]);
    gpgme_release(ctx);
	return EXIT_SUCCESS;
}



